//
//  NSURLRequest+IgnoreSSL.h
//  NxtMac
//

#import <Foundation/Foundation.h>

@interface NSURLRequest (IgnoreSSL)
+(BOOL)allowsAnyHTTPSCertificateForHost:(NSString*)host;
@end
