//
//  AppDelegate.h
//  NxtMac
//

#import <Cocoa/Cocoa.h>
#import <WebKit/WebKit.h>

@class ASIHTTPRequest;


@interface WebInspector : NSObject  { WebView *_webView; }
- (id)initWithWebView:(WebView *)webView;
- (void)detach:     (id)sender;
- (void)show:       (id)sender;
- (void)showConsole:(id)sender;
@end


@class WebSecurityOrigin;

@interface WebStorageManager : NSObject
+ (WebStorageManager *)sharedWebStorageManager;
// Returns an array of WebSecurityOrigin objects that have LocalStorage.
- (NSArray *)origins;
- (void)deleteAllOrigins;
- (void)deleteOrigin:(WebSecurityOrigin *)origin;
- (void)syncLocalStorage;
- (void)syncFileSystemAndTrackerDatabase;
  static NSString* _storageDirectoryPath();
+ (NSString *)_storageDirectoryPath;
@end

@interface WebPreferences (WebPreferencesPrivate)
- (void)_setLocalStorageDatabasePath:(NSString *)path;
- (void) setLocalStorageEnabled: (BOOL) localStorageEnabled;
- (BOOL)databasesEnabled;
- (void)setDatabasesEnabled:(BOOL)databasesEnabled;
- (NSString *)_localStorageDatabasePath;
- (BOOL)localStorageEnabled;
- (int64_t)applicationCacheTotalQuota;
- (void)setApplicationCacheTotalQuota:(int64_t)quota;
- (int64_t)applicationCacheDefaultOriginQuota;
- (void)setApplicationCacheDefaultOriginQuota:(int64_t)quota;
- (BOOL)webGLEnabled;
- (void)setWebGLEnabled:(BOOL)enabled;
- (BOOL)offlineWebApplicationCacheEnabled;
- (void)setOfflineWebApplicationCacheEnabled:(BOOL)offlineWebApplicationCacheEnabled;
- (BOOL)developerExtrasEnabled;
- (void)setDeveloperExtrasEnabled:(BOOL)flag;

@end


@interface AppDelegate : NSObject <NSApplicationDelegate, NSMenuDelegate>

{
    IBOutlet WebView *_webView;
    IBOutlet NSMenuItem *_checkForUpdates;
    IBOutlet NSMenuItem *_switchNet;
    
    ASIHTTPRequest *_bigFetchRequest;
    ASIHTTPRequest *_bigFetchRequest2;
}

@property (assign) IBOutlet NSWindow *window;
@property (nonatomic) ASIHTTPRequest *bigFetchRequest;
@property (nonatomic) ASIHTTPRequest *bigFetchRequest2;

- (IBAction)checkForUpdates:(id)sender;
- (IBAction)redownloadBlockChain:(id)sender;
- (IBAction)switchNet:(id)sender;
- (IBAction)viewServerLog:(id)sender;
- (IBAction)showPreferencesWindow:(id)sender;

- (IBAction)openNxtForumUrl:(id)sender;
- (IBAction)openWikiUrl:(id)sender;
- (IBAction)openIRCUrl:(id)sender;
- (IBAction)openNxtUrl:(id)sender;

@end
