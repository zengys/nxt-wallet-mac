//
//  NSTextField+Hyperlink.m
//  NxtMac
//

#import "NSTextFieldLink.h"

@implementation NSTextFieldLink

- (void) resetCursorRects {
    [self addCursorRect:[self bounds] cursor:[NSCursor pointingHandCursor]];
}

- (void)mouseUp:(NSEvent *)theEvent {
    [super mouseUp:theEvent];
    /*
    if (_delegateObj != nil && [_delegateObj respondsToSelector:@selector(donate:)]) {
        [_delegateObj performSelector:@selector(donate:) withObject:nil];
    }*/
}

@end
